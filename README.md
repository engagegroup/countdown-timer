# Countdown Timer

Originally deployed on DOW's Adoption Center, adds a countdown to the top of the page.

### How do I get set up? ###

* Copy content of Countdown.html to the page on which it should appear.
* Update the first script tag with the path to countdown.min.js for your implementation.
* Update the styles and text to suit the design requirments

### Who do I talk to? ###

Paul B. Joiner paulj@engageyourcause.com